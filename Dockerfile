FROM maven:3.6.3-jdk-11

ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 11.15.0

RUN apt-get update -qq
RUN apt-get install xvfb firefox-esr -yqq --force-yes

RUN mkdir $NVM_DIR

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN apt-get update \
    && apt-get install -y curl vim git-core \
    && apt-get -y autoclean \
    && apt-get install zip

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

RUN source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN npm install yarn -g

CMD [ "/bin/bash" ]

